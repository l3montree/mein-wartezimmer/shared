import { Organization } from "./index";

export interface Token {
    refreshToken: string;
    accessToken: string;
    tokenType: "Bearer";
    expiresIn: number;
}

export interface SignupRequest extends Omit<Organization, "id" | "slug"> {
    sections: string[];
    password: string;
}

export interface SyncResponse extends Organization {
    sections: Section[];
}

export interface RefreshTokenRequest {
    refreshToken: string;
}

export interface Section {
    title: string;
    id: number;
}

export interface LoginRequest {
    email: string;
    password: string;
}
