export enum TicketType {
    usual,
    // 5 minutes
    smallDelay = 5,
    // 10 minutes
    mediumDelay = 10,
    // 30 minutes
    bigDelay = 30,
}

export interface OpenStreetMapResponse {
    lat: string;
    lon: string;

    display_name: string;

    address: AddressDetails;
}

export interface AddressDetails {
    house_number?: string;
    road?: string;
    town?: string;
    postcode?: string;
    country?: string;
    country_code?: string;
}

export enum SortFlag {
    last = -2,
    first,
}

export enum TicketState {
    pending,
    finished,
    deleted,
}

export interface Paginated<T> {
    content: T[];
    numberOfElements: number;
    empty: boolean;
    totalPages: number;
    totalElements: number;
    // current page.
    currentPage: number;
}

export interface Organization extends AddressDetails {
    title: string;
    address: string;
    lat: string;
    lon: string;
    phoneNumber: string;
    averageTimeNeeded: number;
    leadTime?: number;
    email: string;
    id: number;
    publicAccessible: boolean;
    slug: string;
    password?: string;
}

export interface Ticket {
    id: number;
    state: TicketState;
    title: string;
    finishedAt?: Date | string;
    createdAt: Date | string;
    position: number;
    type?: TicketType;
}
